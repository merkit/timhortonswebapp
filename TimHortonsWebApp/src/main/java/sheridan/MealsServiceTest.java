package sheridan;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testDrinksRegular() {
		MealsService mc = new MealsService();
		assertTrue("Wrong", !mc.getAvailableMealTypes(MealType.DRINKS).isEmpty());
	}
	
	@Test
	public void testDrinksException() {
		MealsService mc = new MealsService();
		List<String> a = mc.getAvailableMealTypes(null);
		assertTrue("Wrong", a.get(0).equalsIgnoreCase("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		MealsService mc = new MealsService();
		assertTrue("Wrong", mc.getAvailableMealTypes(MealType.DRINKS).size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService mc = new MealsService();
		assertTrue("Wrong", mc.getAvailableMealTypes(null).size() <= 1);
	}
	
	

}
